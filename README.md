![EZ!](/resources/images/ez-smiley-small-logo.png)
# ArgoCD deployment

## Manual deployment of ArgoCD in the ```cicd``` namespace

(The instructions were created according to this post: https://github.com/yorammi/ez-cicd-learning/blob/master/learning-workspace-setup/argocd-installation.md)

### Requirements
- Installed kubectl command-line tool.
- Connected to a Kubernetes cluster - Have a kubeconfig file (default location is ~/.kube/config).
- Create the ```cicd``` namespace if not exists already. You can do it by running the following command:
```
kubectl create namespace cicd
```
 
### Install Argo CD

Run the following command to deploy ArgoCD in the ```cicd``` namespace
```
kubectl apply -n cicd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

Get the password from the new terminal using this command:
```
kubectl -n cicd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d
```

If the ArgoCD service is not exposed with the ingress as ```http://<IP-address or server-name>/cd```, you can test the service by:

1. Changing the argocd-server service type to LoadBalancer:
```
kubectl patch svc argocd-server -n cicd -p '{"spec": {"type": "LoadBalancer"}}'
```

2. access to the Argo CD UI from your browser by typing the following URL:
```
http://<IP-address or server-name>
```
where the IP is the external-service-IP of the ```argocd-server``` service

If exposed with the ingress, you can access it in this URL: ```http://<IP-address or server-name>/cd```

In both cases:

Login to Argo CD UI using the username: ```admin``` and and the above password

## Manual removal of ArgoCD from the ```cicd``` namespace

**This section will be filled when the students will find out how to do it. I won't make this ![EZ!](/resources/images/ez-smiley-small-logo.png) for them!**
